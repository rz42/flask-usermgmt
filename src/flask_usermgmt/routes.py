import flask
from flask import render_template
from flask_login import login_required, logout_user, fresh_login_required
from . import login, chpw

def create_root_routes(app):
    @app.route("/hello")
    def hello():
        return "Hello, World!"

    @app.route("/")
    def default():
        return landing_page()

    def landing_page():
        return render_template('landing_page.html')

    @app.route("/password_reset")
    def password_reset():
        return "password reset"

    @app.route("/password_change", methods=['GET', 'POST'])
    @fresh_login_required
    def passwort_change():
        return chpw.chpw_route()

    @app.route("/new_registration")
    def new_registration():
        return "new registration"

    @app.route("/admin")
    @login_required
    def admin():
        return "admin"

    @app.route('/login', methods=['GET', 'POST'])
    def login_route():
        return login.login_route()

    @app.route('/logout')
    def logout_route():
        logout_user()
        return 'Logged out'

    return app
