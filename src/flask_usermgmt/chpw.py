from wtforms.validators import DataRequired, EqualTo, InputRequired
from flask import render_template
from flask_wtf import FlaskForm
from wtforms import PasswordField, HiddenField
import flask_login
import flask
from flask import url_for

class ChangePasswordForm(FlaskForm):
    next = HiddenField('/')
    password = PasswordField("New Password", validators=[InputRequired(), EqualTo("password_confirmation", "Password and Confirmation do not match!")])
    password_confirmation = PasswordField("Repeat Password", validators=[InputRequired()])

def chpw_route():
    form = ChangePasswordForm()
    if form.validate_on_submit():
        user = flask_login.current_user
        conn = user.connection
        user_dn = conn.extend.standard.who_am_i()
        success = conn.extend.standard.modify_password(user.user_dn, None, form.password.data)
        print(success)
        flask.current_app.login_manager.needs_refresh()
        if success:
            flask.flash("Password change successful!", "info")
        else:
            flash.flash("Password change failed!", "danger")
        return flask.redirect(url_for("default"))
    return render_template("password_change.html", form=form)
