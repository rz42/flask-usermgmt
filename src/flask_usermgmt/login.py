import flask
from flask import render_template
from flask_login import UserMixin, LoginManager, login_user
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, HiddenField
from wtforms.validators import DataRequired

import ldap3
import ssl

users = {}


class LoginForm(FlaskForm):
    next = HiddenField('/')
    name = StringField("Username", validators=[DataRequired()])
    password = PasswordField("Password", validators=[DataRequired()])



def create_connection(config, bind_user, bind_pw):
    if config["protocol"] == "ldaps":
        validation = ssl.CERT_REQUIRED
        if config["tls_insecure"]:
            validation = ssl.CERT_NONE
        tls = ldap3.Tls(
            validate=validation,
            ca_certs_file=ssl.get_default_verify_paths().cafile,
            version=ssl.PROTOCOL_TLSv1_2,
        )
    conn = ldap3.Connection(
        server=ldap3.Server(
            config["protocol"] + "://" + config["server"] + ":" + str(config["port"]),
            tls=tls,
        ),
        user=bind_user,
        password=bind_pw,
    )
    if not conn.bind():
        return None
    return conn


def get_dn(config, connection, name_or_email):
    search_filter = f"(|(uid={name_or_email})(mail={name_or_email}))"
    connection.search(config["user_dn"], search_filter)
    if len(connection.response) != 1:
        return None
    return connection.response[0]["dn"]


def get_user_info(config, connection, dn):
    search_filter = "(objectClass=*)"
    connection.search(
        dn,
        search_filter=search_filter,
        search_scope=ldap3.BASE,
        attributes=["cn", "sn", "mail", "uid"],
    )
    results = connection.response[0]
    return results


class User(UserMixin):
    def __init__(self, connection, user_dn, fullname, username, email=None):
        super(User, self).__init__()
        self.connection = connection
        self.user_dn = user_dn
        self.fullname = fullname
        self.username = username
        self.email = email
    def get_id(self):
        return self.user_dn


def login_route():
    form = LoginForm()
    if form.validate_on_submit():
        conn = create_login(
            flask.current_app.login_manager,
            flask.request.form["name"],
            flask.request.form["password"],
        )
        if conn is None:
            flask.flash("Username or password is wrong", "danger")
            return render_template("login.html", form=form)
        user_info = get_user_info(
            flask.current_app.login_manager.config, conn, conn.user
        )
        user = User(
            conn,
            user_info["dn"],
            user_info["attributes"]["cn"],
            user_info["attributes"]["uid"],
            user_info["attributes"]["mail"],
        )
        login_user(user)
        users[user.user_dn] = user
        return flask.redirect(
            form.next.data or flask.url_for("default")
        )
    if form.errors:
        flask.flash(form.errors, "danger")
    form.next.data = flask.request.args.get('next')
    return render_template("login.html", form=form)


def create_loginmgr(app):
    login_manager = LoginManager()
    login_manager.init_app(app)

    # Put config for ldap into login_manager
    login_manager.config = app.config["LDAP"]
    # Create central connection for user_mgmt
    conn = create_connection(
        login_manager.config,
        login_manager.config["bind_dn"],
        login_manager.config["bind_pw"],
    )
    if conn is None:
        raise ValueError("Connection to LDAP server is not possible")
    login_manager.conn = conn
    login_manager.login_view = "login_route"
    login_manager.refresh_view = "login_route"

    @login_manager.user_loader
    def load_users(dn):
        if dn not in users:
            return
        return users[dn]

    return app

    @login_manager.unauthorized_handler
    def send_to_login():
        return flask.redirect(flask.url_for("login_route"))


def create_login(login_manager, name, password):
    # We not necessarily get the actual name, we need to find the proper DN
    dn = get_dn(login_manager.config, login_manager.conn, name)
    if dn is None:
        return None
    conn = create_connection(login_manager.config, dn, password)
    return conn
