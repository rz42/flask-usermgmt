import os

import toml
from flask import Flask
from flask_bootstrap import Bootstrap
from flask_login import LoginManager


from . import routes, login


def create_app(test_config=None, instance_path=None):
    # create and configure the app
    app = Flask(__name__, instance_relative_config=True, instance_path=instance_path)

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_file("config.toml", load=toml.load)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)

    Bootstrap(app)
    from . import db
    db.init_db(app)
    app = login.create_loginmgr(app)

    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
    )

    app = routes.create_root_routes(app)


    return app
