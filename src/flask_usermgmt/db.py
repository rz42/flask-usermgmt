import flask
from sqlalchemy import create_engine, Column, Integer, String, DateTime
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()

def init_db(app):
    engine = create_engine('sqlite:///' + app.instance_path + '/' + app.config['SQLALCHEMY_DATABASE_FILE'])
    app.engine = engine
    app.db_session = scoped_session(sessionmaker(autocommit=False,
                                             autoflush=False,
                                             bind=engine))

    Base.query = app.db_session.query_property()
    Base.metadata.create_all(bind=engine)


class UserRegistration(Base):
    __tablename__ = 'userregistration'
    id = Column(String(25), primary_key=True)
    valid_until = Column(DateTime, nullable=False)

class PasswortReset(Base):
    __tablename__ = 'passwordreset'
    id = Column(String(25), primary_key=True)
    valid_until = Column(DateTime, nullable=False)
    user_dn = Column(String(120), nullable=False)
